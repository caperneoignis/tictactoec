﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace TicTacToe
{
    class ComputerPlayer : Player
    {
       private LCHashMap<Board, Pair> hashMap_ = new LCHashMap<Board,Pair>();
        const int HASH_MAP_MOVES = 197000;
        enum gameState {O_WINS = -1, DRAW = 0, X_WINS = 1};
        private Board.Piece playerPiece_;
        public ComputerPlayer(Board.Piece player)
        {	
            playerPiece_ = player;
             fillHashMap();
        }
        /*fills the hashmap with the different boards*/
        public LCHashMap<Board, Pair> fillHashMap()
        {
            //Tuple<int,int> item;
            //Pair item = new Pair();
	        LCHashMap<Board, Pair> temp = new LCHashMap<Board,Pair>();
	        Board currentBoard = new Board();
	        Pair bestSelection = new Pair();
	        Board.Piece playPiece = Board.Piece.X;
	        do{
		        if(playPiece == Board.Piece.X)
		        {
				        //max move calculation
				        xMoveCalculation(currentBoard, bestSelection, temp);
		        }
		        else
		        {
				        //min move calculation
				        oMoveCalculation(currentBoard, bestSelection, temp);
		        }
		        temp[currentBoard] = bestSelection;
		        //insert best move into board.
		        currentBoard.playPiece((int)bestSelection.First,(int) bestSelection.Second, playPiece);
		        if(playPiece == Board.Piece.O){
			        playPiece = Board.Piece.X;
		        }
		        else{
			        playPiece = Board.Piece.O;
		        }
	        }while(!currentBoard.isFull());
            hashMap_ = temp;
	        return temp;
        }

        public override void makeNextMove(Board currentBoard)
        {
	        currentBoard.playPiece((int)hashMap_[currentBoard].First, (int)hashMap_[currentBoard].Second, playerPiece_);
	        //currentBoard.drawBoard();
        }
        /* make every move possible and then decide the best move based on the outcome of that move
        * go all the way through to conclusion of that move on the current board, empty board go all the way to the end of all 
        * possible moves*/
        public int xMoveCalculation(Board currentBoard, Pair bestPair, LCHashMap<Board, Pair> temp)
        {
	        Pair dontCare = new Pair();
	        int maxScore = -1;
	        int currentScore;
	        Board.State currentState = currentBoard.gameState();
	        if(currentBoard.isFull() && currentState != Board.State.OWINS){
	           maxScore = 0;
	        }
	        else if(currentState == Board.State.OWINS){
		        maxScore = -1;
	        }
	        else if (!(notImmediateWin(currentBoard, Board.Piece.X, bestPair))){
		        maxScore = 1;
	        }
	        else{
		        for(int row = 0; row < Board.MAX_ROWS; row++){
			        for(int column = 0; column < Board.MAX_COLUMNS; column++){
				        if(currentBoard.isSpotEmpty(row, column)){
					        currentBoard.playPiece(row, column, Board.Piece.X);
					        currentScore = oMoveCalculation(currentBoard, dontCare, temp);
					        currentBoard.removePiece(row, column);
					        if ( currentScore > maxScore){
						        //set max score, then set row to column to reflect new values for first best move
						        maxScore = currentScore;
						        bestPair.First = row;
						        bestPair.Second = column; 
					        }
					        //insert for certian occasions
					        temp.insert(currentBoard, bestPair);
				        }
			        }
		        }
	        }
	        //store
	        temp[currentBoard] = bestPair;
	        return maxScore;
        }
        /* easiest, and cleanest, implemenation we could find to find all possible positions by the computer programs */ 
        //will return worst case of move -1, 0, 1
        int oMoveCalculation(Board potentialBoard, Pair bestPair, LCHashMap<Board, Pair> temp)
        {
	        int minScore = 1;
	        int currentScore;
	        Pair dontCare = new Pair();
	        Board.State currentState = potentialBoard.gameState();
	        if(potentialBoard.isFull() && currentState != Board.State.XWINS){
	           minScore = 0;
	        }
	        else if(currentState == Board.State.XWINS ){
		        minScore = 1;
	        }
	        else if (!(notImmediateWin(potentialBoard, Board.Piece.O, bestPair))){
		        minScore = -1;
	        }
	        else{
		        for(int row = 0; row < Board.MAX_ROWS; row++){
			        for(int column = 0; column < Board.MAX_COLUMNS; column++){
				        if(potentialBoard.isSpotEmpty(row, column)){
					        potentialBoard.playPiece(row, column, Board.Piece.O);
					        currentScore = xMoveCalculation(potentialBoard, dontCare, temp);
					        //remove the piece after finding it's score
					        potentialBoard.removePiece(row, column);
					        if ( currentScore < minScore){
						        //set min score, then set row to column to reflect new values for first best move
						        minScore = currentScore;
						        bestPair.First = row;
						        bestPair.Second = column; 
					        }
					        //every possible board we want best move
					        temp.insert(potentialBoard, bestPair);
				        }
			        }
		        }
	        }
	        //store
	        temp[potentialBoard] = bestPair;
	        return minScore;
        }
        bool notImmediateWin(Board currentBoard, Board.Piece gamepiece, Pair instantWin)
        {
	        Board.State currentState = currentBoard.gameState();
	        int row = 0;
	        int column = 0;
	        while (row < Board.MAX_ROWS && currentState == Board.State.INCOMPLETE){
		        while (column < Board.MAX_COLUMNS && currentState == Board.State.INCOMPLETE ){
			        if(currentBoard.isSpotEmpty(row, column)){
				        currentBoard.playPiece(row, column, gamepiece);
				        currentState = currentBoard.gameState();
				        //remove after finding the score
				        currentBoard.removePiece(row, column);
				        if ( gamepiece == Board.Piece.X && currentState == Board.State.XWINS){
					        instantWin.First = row;
					        instantWin.Second = column;
				        }
				        else if ( gamepiece == Board.Piece.O && currentState == Board.State.OWINS ){
					        instantWin.First = row;
					        instantWin.Second = column;
				        }
			        }
			        column++;
		        }
		        column = 0;
		        row++;
	        }
	        //if it isn't an immediate win return false and go into min max.
	        return currentState == Board.State.INCOMPLETE || currentState == Board.State.DRAW;
        }
    }
}
