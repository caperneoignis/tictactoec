﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    class Equator: Board
    {
        public bool equals_(Board LHS, Board RHS)
	    {
		    //== operator is already written in board.cpp
		    return LHS == RHS;
	    }
    }
}
