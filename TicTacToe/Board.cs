﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    //todo add override for the equals function
    class Board
    {
        protected List<List<char>> gameBoard_;
        protected char gamePiece_;
        public const char xPiece_ = 'X';
        public const char oPiece_ = 'O';
        public const char empty_ = '_';
        public enum Piece{X=0, O, NONE};
		public const int MAX_ROWS = 3;
		public const int MAX_COLUMNS = 3;
		public const int PIECES = 3;
		public enum State{XWINS, OWINS, DRAW, INCOMPLETE};
        //defualt constructor
        public Board()
        {
            gamePiece_ = empty_;
            //temporary list to fill up 2d list
	        
	        gameBoard_= new List<List<char>>(MAX_COLUMNS);
            for(int i = 0; i < MAX_ROWS; i++)
            {
                List<char> letterInsert = new List<char>(MAX_COLUMNS);
		        for(int j = 0; j < MAX_COLUMNS; j++)
                {	
			        letterInsert.Add(pieceConversionReturn(Piece.NONE));
                }
		        gameBoard_.Add(letterInsert);
	            //letterInsert.Clear();
            }
        }
        //copy constructor
        public Board(Board rhs)
        {
            for (int i = 0; i < MAX_ROWS; i++)
            {
                for (int j = 0; j < MAX_COLUMNS; j++)
                {
                    this.gameBoard_[i][j] = rhs.gameBoard_[i][j];
                }
            }
        }
        ~Board()
        {

        }
        public Board.Piece checkPiece(int row,int column)
        {
	        Piece retVal = Board.Piece.NONE;
	        if (gameBoard_[row][column] == xPiece_){
	        retVal = Board.Piece.X;
	        }
	        else if (gameBoard_[row][column] == oPiece_){
	        retVal = Board.Piece.O;
	        }
	        else{
		        //do nothing the spot is empty
	        }
	        return retVal;
        }
        //print state, more for debugging
        string printState(Board.State state)
        {
          string retVal = "Incomplete";
          switch(state)
            {
            case Board.State.OWINS:
              {
	        retVal = "O wins";
	        break;
              }
            case Board.State.XWINS:
              {
	        retVal = "X wins";
	        break;
              }
            case Board.State.DRAW:
              {
	        retVal = "Draw";
	        break;
              }
            case Board.State.INCOMPLETE:
              retVal = "Incomplete";
              break;
            }
          return retVal;
        }
        //operator == overload
        public static bool operator ==(Board RHS, Board LHS)
        {

          //work on later
          //bool isEqual = true;
	        int row = 0;
	        int column = 0;
	        while (row < MAX_ROWS && LHS.checkPiece(row,column) == RHS.checkPiece(row, column))
	        {
		        column = 0;
		        while (column < MAX_COLUMNS && LHS.checkPiece(row,column) == RHS.checkPiece(row, column))
		        {
			        ++column;
		        }
		        ++row;
	        }
          return (row >= MAX_ROWS);
        }
        //have to have the inverse in c#
        public static bool operator!=(Board rhs, Board lhs)
        {
          return !(lhs.gameBoard_ == rhs.gameBoard_);
        }
        //place the piece if it is a legal move
        public void playPiece(int row, int column, Piece piece)
        {
	        if ( !isSpotEmpty (row, column) )
	        {
		        //throw IllegalMoveException();
	        }
	        else
	        {
		        gameBoard_[row][column] = pieceConversionReturn(piece);
	        }
        }
        //remove piece at spot.
        public void removePiece(int row, int column)
        {
	        gameBoard_[row][column] = pieceConversionReturn((Board.Piece.NONE));
        }
        //check to see if board is full
       public bool isFull()
        {
          int takenSpaces = 0;
          for(int i = 0; i < MAX_ROWS; i++){
		        for(int j = 0; j < MAX_COLUMNS; j++){
		          if(gameBoard_[i][j] != pieceConversionReturn(Board.Piece.NONE)){
			          takenSpaces++;
			        }
		        }
          }
          return (takenSpaces == (MAX_COLUMNS * MAX_ROWS) );
        }
        public Board.State gameState() 
        {
          Board.State gameState = Board.State.INCOMPLETE; 
  
          //IF XWINS - 0
          //gameState = Board::XWINS;
          //first row straight across
          int rowMover = 0;
          int columnMover = 0;
          int columnInt = 1;
          int columnDec = -1;
          int rowInt = 1;
          int upperRightCorn = 2;
          //row straight across
           while(gameState == Board.State.INCOMPLETE && rowMover < MAX_ROWS){
            if(CheckLine(rowMover,0,0,columnInt)){
              if(  gameBoard_[rowMover][0] == xPiece_){
		        gameState = Board.State.XWINS;
              }
              else if (  gameBoard_[rowMover][0] == oPiece_){
		        gameState = Board.State.OWINS;
              }
            }
            rowMover++;
            //cout << "we are straight" <<endl;
          }
          //now check columns
          while(gameState == Board.State.INCOMPLETE && columnMover < MAX_COLUMNS){
            if(CheckLine(0,columnMover,rowInt,0)){

              if(  gameBoard_[0][columnMover] == xPiece_){
			        gameState = Board.State.XWINS;
              }
              else if (  gameBoard_[0][columnMover] == oPiece_){
			        gameState = Board.State.OWINS;
              }
            }
	        columnMover++;
          }
          //check slent
          if(gameState == Board.State.INCOMPLETE && CheckLine(0,0,rowInt,columnInt)){
            if(  gameBoard_[0][0] == xPiece_){
              gameState = Board.State.XWINS;
            }
            else if (  gameBoard_[0][0] == oPiece_){
              gameState = Board.State.OWINS;
            }
      
          }
          if(gameState == Board.State.INCOMPLETE && CheckLine(0,upperRightCorn, rowInt, columnDec)){
	        if(  gameBoard_[0][upperRightCorn] == xPiece_){
	          gameState = Board.State.XWINS;
	        }
	        else if (  gameBoard_[0][upperRightCorn] == oPiece_) {
	          gameState = Board.State.OWINS;
	        }
          }
          int fullRowMover = 0;
         if( isFull() ){
            if(CheckLine(fullRowMover,0,0,columnInt)){
              if(  gameBoard_[fullRowMover][0] == xPiece_){
			        gameState = Board.State.XWINS;
              }
	          fullRowMover++;
	        }
	        else if ( gameState != Board.State.XWINS ){
		        gameState = Board.State.DRAW;
	        }
          }
          return gameState;
        }
        //check to see if line has three in a row.
        private bool  CheckLine(int startRow, int startColumn, int rowIn, int columnIn)
        {
	        int rowCursor = startRow + rowIn;
	        int columnCursor = startColumn + columnIn;
	        int incrementCounter = 0;
	        int completeLine = 2;
            while (goodPosition(rowCursor, columnCursor) && gameBoard_[startRow][startColumn] == gameBoard_[rowCursor][columnCursor] && gameBoard_[rowCursor][columnCursor] != pieceConversionReturn(Board.Piece.NONE))// && gameBoard_[rowCursor][columnCursor] != empty_)
            {
                rowCursor += rowIn;
                columnCursor += columnIn;
                incrementCounter++;
            }
	        return (incrementCounter == completeLine);
        }
        //if the position is good return true
        private bool goodPosition(int row, int column)
        {
          return ((row < MAX_ROWS && row >= 0) && (column < MAX_COLUMNS && column >= 0));// && isSpotEmpty(row , column);
        }
        //ditto but the opposite of the above
        public bool isSpotEmpty(int row, int column)
        {
          return ( gameBoard_[row][column] == pieceConversionReturn(Board.Piece.NONE) );
        }
        //coverts the peice to it's char repsentation
        char pieceConversionReturn(Piece piece)
        {
	        char charConvert = empty_;
            switch(piece)
            {
            case Board.Piece.X:
              charConvert = xPiece_;
              break;

            case Board.Piece.O:
              charConvert = oPiece_;
              break;

            case Board.Piece.NONE:
              charConvert = empty_;
              break;
            }
	        return charConvert;
        }
    }
}
