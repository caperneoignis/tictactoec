﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToe
{
    public class LCHashMap < K, V>
        where K : class, new()
        where V : class, new()
    {
	/* constructor */
	public LCHashMap(int size = 200)
	{
        //make the initial array of a certian size then make all elements empty
        hashTable_ = new List<HashEntry>(nextPrime(size));
		makeEmpty();
	}
    public V this [K key]
    {
        get
        {
            int index = findPos(key);
            if (!isActive(index))
            {
                insert(key, new V());
            }

            return hashTable_[index].value_;
        }
        set
        {
            int index = findPos(key);
            if (!isActive(index))
            {
                insert(key, value);
            }
            else
            {
                hashTable_[index].value_ = value;
            }
        }
    }
	/* cleans up all memory for storage and calls the destructor for the
	   keys and values stored */
	~LCHashMap()
	{
		hashTable_.Clear();
	}
	/* inserts the key value pair */
	public bool insert(K key, V value)
	{
	  	int currentPos = findPos(key);
		bool wasInserted = false;
		if(!isActive(currentPos))
		{
			//we enetered so it was inserted
			wasInserted = true;
			hashTable_[currentPos] = new HashEntry(key, value,LCHashMap<K,V>.EntryType.ACTIVE);
			++currentSize_;
			++load_;
			if( currentSize_ > hashTable_.Count * lamda_)
			{
				rehash();
			}
		}
		return wasInserted;
	}
	/* erases key value pair referenced by key */
	public bool erase( K key)
	{
		int index = findPos(key);
		//if it wasn't already deleted do this.
		bool wasDeleted = false;
		if(isActive(index))
		{
			wasDeleted = true;
			hashTable_[index].info_ = LCHashMap<K,V>.EntryType.DELETED;
			--currentSize_;
			//we don't update the load here just size. 
		}	
		return wasDeleted;
	}

	/* lookup the value associated with a key. if the key is not in the
	map, insert it with default value. Should provide l-value access to
	value.*/
	public V  index(K key)
	{
		int index = findPos(key);
		if(!isActive(index))
		{
            Type type = typeof(V);
			insert(key, new V());
		}
		return hashTable_[index].value_;
	}

	/* returns true if this key maps to a value */
	public bool isIn (K key)
	{
	  	int index = findPos(key);
		return isActive(index); 
	}

	/* returns true if the map is empty */
	public bool empty()
	{
		return size() == 0;
	}

	/* number of key value pairs stored */
	public int size()
	{
		return currentSize_;
	}

	/* empties the map */
	public void clear()
	{
		hashTable_.Clear();
	}
	//enum types
	public enum EntryType { ACTIVE, EMPTY, DELETED };
	public class HashEntry : LCHashMap< K, V>
    {	
	        public HashEntry(K key, V value, EntryType info)
            {
                key_ = key;
                value_ = value;
                info_ = info;
            }
			public K key_;
			public V value_;
			public EntryType info_;
	}
	//returns if the position is active or not
	public bool isActive(int currentPos)
	{
		return hashTable_[currentPos].info_ == LCHashMap<K,V>.EntryType.ACTIVE;
	}
	//*************private variables**************
	//protected HashFn hashFunction_;
	//protected Equator equals_;
	protected List<HashEntry> hashTable_;
	protected int currentSize_;
	protected int load_;
	protected const float lamda_ = .5f;
	//******************end of section************************
	//find the position the key should be or is at
	public int findPos( K key)
	{
		int offset = 1;
		int quadAdd = 2;
        //if the Key is of the type board then use it. else throw.
        Board hash = new Board();
        try {
            if (key.GetType() == typeof(Board))
            {
                hash = (Board)(object)key;
            }
        }
        catch(Exception e)
        {
            MessageBox.Show(e.ToString());
        }
        finally
        {
            hashTable_.Clear();
            System.Environment.Exit(0);
        }
		//current postion is hash function mod the size
		int currentPos = HashFunction.hash(hash)  % hashTable_.Count();
		//find if the location is empty, and if the values equals.
		while(hashTable_[currentPos].info_ != LCHashMap<K,V>.EntryType.EMPTY && (hashTable_[currentPos].key_ != key))
		{
			currentPos += offset;
			offset += quadAdd;
			if(currentPos >= hashTable_.Count() )
			{
				currentPos -= hashTable_.Count();
			}
			
		}
		return currentPos;
	}
	//finds the next prime 
	private int nextPrime(int currentSize)
	{
		//lets find a big number to reduce rehases
		int prime = currentSize * 2 - 1; //lets increase by two at the start
		int onlyEvenPrime = 2;
		while(!isPrime(prime))
		{
			prime += onlyEvenPrime; //always increment by 2 so we stay odd
		}
		//return the result
		return prime;
	}
	//member function of the find next prime
	private bool isPrime(int potentialPrime)
	{
		//first none even prime
		int increment = 3;
		//divide the number by three
		int divisor = potentialPrime / increment;
		/*increment by two so we stay odd and don't get a divisor that is
		 * also a multipul of two which means the number is also dividable by two
		 * and would have already been caught */
		int onlyEvenPrime = 2;
		/*while divisor is less then increment keep dividing and seeing 
		 * if the divisor times the increment equals the prime
		 * if it does it is not a prime */
		while(increment < potentialPrime && potentialPrime != (divisor * increment))
		{
			//increment by two to stay odd
			increment += onlyEvenPrime;
			/*will have a round off error so divisor times increment
			* should not equal when multipulied togather since divisor
			* was rounded down */
			divisor = potentialPrime / increment;
		}
		//if the while loop broke early should be false
		return increment >= potentialPrime;
	}
	//rehash if the current size and array size is within lamda constrants
	private void rehash()
	{
		List<HashEntry> oldArray = hashTable_;
		hashTable_ = new List<HashEntry>( nextPrime( 2 * oldArray.Count() ) );
		for (int j = 0; j < hashTable_.Count(); j++)
		{
            hashTable_[j].info_ = LCHashMap<K, V>.EntryType.EMPTY;
		}
		load_ = 0;
		currentSize_ = 0;
		for(int i =0; i < oldArray.Count(); i++)
		{
            if (oldArray[i].info_ == LCHashMap<K, V>.EntryType.ACTIVE)
			{
				insert(oldArray[i].key_, oldArray[i].value_);
			}
		}
	}
	public void makeEmpty()
	{
		currentSize_ = 0;
		load_ = 0;
		for( int i = 0; i < hashTable_.Count(); i++)
		{
            hashTable_[i].info_ = LCHashMap<K, V>.EntryType.EMPTY;
		}
	}
    }
}
