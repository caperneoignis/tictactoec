﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    class HashFunction
    {
        static public int hash (Board boardToHash)
		{
			int hash = 0;
			Board.Piece pieceOnSquare;
			//make a big number by adding up all the pieces on the board.
			for(int row = 0; row < Board.MAX_ROWS; row++){
				for(int column = 0; column < Board.MAX_COLUMNS; column++){
					pieceOnSquare = boardToHash.checkPiece(row, column);
					if(pieceOnSquare == Board.Piece.X){
						hash +=(int) Math.Pow(3 + row + column, 3 * row + column) * 3;
					}
					else if (pieceOnSquare == Board.Piece.O){
						hash +=(int) Math.Pow(3 + row + column, 3 * row + column) * 2;
					}
				}
			}
			return hash;
		}
    }
}
